import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.grimtools.com/calc/vNQYE38N')

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/button_Zaakceptuj (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Edit (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.doubleClick(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.doubleClick(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Cunning_stat-decrease (1)'))

WebUI.doubleClick(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.click(findTestObject('Object Repository/GrimDawn/Page_Cabalist, Level 100 (GD 1.1.2.2) - Gri_638a75/div_Physique_stat-increase'))

WebUI.closeBrowser()

