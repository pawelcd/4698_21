import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.wielkiewina.pl/panel/login')

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Logowanie - Wielkie Wina/a_ZAREJESTRUJ SI'))

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorName'), 
    'Jan')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorSurname'), 
    'Nowak')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorBirthdate'), 
    '01.01.1980')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorStreet'), 
    'testowa')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorHouseNumber'), 
    '1')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorApartmentNumber'), 
    '1')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorPostCode'), 
    '00-000')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorCity'), 
    'testowo')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Dane Klienta_distributorPhone'), 
    '813813813')

WebUI.setText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Na ten adres zostanie wysany link aky_1cc683'), 
    'test@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Haso_distributorPass'), 
    'c2qKsy47e3Tzuu8y4/oIJw==')

WebUI.setEncryptedText(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/input_Haso_distributorRepass'), 
    'c2qKsy47e3Tzuu8y4/oIJw==')

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/label_'))

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/label_'))

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/button_zarejestruj si'))

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/label_'))

WebUI.click(findTestObject('Object Repository/wielkiewina.pl/Page_Rejestracja - Wielkie Wina/button_zarejestruj si'))

WebUI.closeBrowser()

